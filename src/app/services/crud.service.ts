import { Injectable } from '@angular/core';
import { Product } from "../models/products.model";
import { HttpClient, HttpHandler, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";

@Injectable({
	providedIn: 'root'
})
export class CrudService {
	private apiUri: string = 'http://localhost:8081/api/products';
	httpHeaders: HttpHeaders = new HttpHeaders().set('Content-type', 'application/json');

	constructor(private httpClient: HttpClient) { }

	getProducts() {
		return this.httpClient.get(this.apiUri, {
			headers: this.httpHeaders
		}).pipe(catchError(this.errorHandle))
	}

	getProduct(id: string): Observable<any> {
		return this.httpClient.get(`${this.apiUri}/${id}`, {
			headers: this.httpHeaders
		}).pipe(
			map((response: any) => {
				return response || {}
			})
		).pipe(catchError(this.errorHandle))
	}

	createProduct(data: Product): Observable<Object> {
		return this.httpClient.post(this.apiUri, data, {
			headers: this.httpHeaders
		}).pipe(catchError(this.errorHandle));
	}

	updateProduct(id: string, data: Product): Observable<Object> {
		return this.httpClient.put(`${this.apiUri}/${id}`, data, {
			headers: this.httpHeaders
		}).pipe(catchError(this.errorHandle));
	}

	deleteProduct(id: string): Observable<Object> {
		return this.httpClient.delete(`${this.apiUri}/${id}`, {
			headers: this.httpHeaders
		}).pipe(catchError(this.errorHandle));
	}

	errorHandle(error: HttpErrorResponse): Observable<never> {
		let errorMessage: string = '';

		if (error.error instanceof ErrorEvent) {
			errorMessage = error.error.message;
		} else {
			errorMessage = `Error code: ${error.status}. Message: ${error.message}`;
		}

		return throwError(() => {
			errorMessage;
		})
	}
}

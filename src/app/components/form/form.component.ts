import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CrudService} from "../../services/crud.service";
import {Router} from "@angular/router";
import {Product} from "../../models/products.model";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit{
	constructor(private formBuilder: FormBuilder, private crudService: CrudService, private router: Router) {}

	formProduct!: FormGroup;

	@Input()
	modelProduct!: Product;

	@Output()
	submitValues: EventEmitter<Product> = new EventEmitter<Product>();

	ngOnInit() {
		this.formProduct = this.formBuilder.group({
			description: ['',Validators.required],
			price: ['',Validators.required],
			stock: ['',Validators.required],
		})

		if (this.modelProduct !== undefined){
			this.formProduct.patchValue(this.modelProduct);
		}
	}

	onSubmit(){
		console.log('Submit');
		this.submitValues.emit(this.formProduct.value)
	}

}

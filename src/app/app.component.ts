import {Component, OnInit} from '@angular/core';
import { CrudService } from "./services/crud.service";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
	title: string = 'frontend';

	constructor(private crudService: CrudService) {}

	ngOnInit() {
		this.crudService.getProducts().subscribe((response) => {
			console.log(response);
		});
	}
}

import {Component, OnInit} from '@angular/core';
import {CrudService} from "../../services/crud.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../../models/products.model";
import {AlertifyService} from "../../services/alertify.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit{
	id: any;
	model!: Product;

	constructor(private crudService: CrudService, private router: Router, private activatedRoute: ActivatedRoute, private alertifyService: AlertifyService) {}

	ngOnInit() {
		this.id = this.activatedRoute.snapshot.paramMap.get('id');
		this.crudService.getProduct(this.id).subscribe( (response) => {
			this.model = {
				_id: response._id,
				description: response.description,
				price: response.price,
				stock: response.stock
			}
		})
	}

	onSubmit(product: Product){
		this.crudService.updateProduct(this.id, product).subscribe({
			next: () => {
				this.alertifyService.success('¡Updated Product!');
				this.router.navigateByUrl('/');
			},
			error: (error) => {
				this.alertifyService.error(error);
			}
		})
	}}

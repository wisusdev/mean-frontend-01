import {Component, OnInit} from '@angular/core';
import {faCirclePlus, faPen, faTrash } from "@fortawesome/free-solid-svg-icons";
import {CrudService} from "../../services/crud.service";
import {Product} from "../../models/products.model";
import {AlertifyService} from "../../services/alertify.service";

@Component({
	selector: 'app-show',
  	templateUrl: './show.component.html',
  	styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit{
	faCirclePlus = faCirclePlus;
	faPen = faPen;
	faTrash = faTrash;

	products: Product[] = [];

	constructor(private crudService: CrudService, private alertifyService: AlertifyService) {}

	ngOnInit() {
		this.crudService.getProducts().subscribe( (response: any) => {
			this.products = response;
		});
	}

	delete(id: string, index: number){
		this.alertifyService.confirm({
			message: 'Are you sure to delete the product?',
			callback_delete: () => {
				this.crudService.deleteProduct(id).subscribe( (response) => {
					this.products.splice(index, 1);
				})
			}
		});
	}
}
